# SCARED! #

Secured Archive Editor

SCARED! is cli base editor for encrypted tar archive.

It ensure that the archive content is not accessible outside of "editing" session.

Upon editing, SCARED! decrypt, detar the archive and start a shell in a private mounted directory.
At shell exit, the directory content is tared, encrypted and finally cleaned.

### Installation ###

SCARED! requires gpg, tar, sudo and unshare.

### Usage ###

    usage: scared [-h] [-l] [-r] [--reset-gpg-parameters]
		  [--system-temporary-directory] [--no-private-mount]
		  file

    SCARED! secured archive editor.

    positional arguments:
      file

    optional arguments:
      -h, --help            show this help message and exit
      -l, --list            list archive content
      -r, --read-only       open archive read only
      --reset-gpg-parameters
			    reset and create a new gpg parameter set
      --system-temporary-directory
			    use system temporary directory (usually /tmp)
      --no-private-mount    don't use private mount

To edit (or create) a SCARED! file, simply run:

    $ scared path/to/your/file

It uses sudo to gain enough priviledge for private mounting a temporary directory:

    [sudo] password for test

(Priviledges are dropped after mounting as been done.)

For a new file, SCARED! queries a few question about encryption parameters:

    No encryption parameters. Creating parameter set.
    Protect with Symmetric, Public-key cryptography or Both ? (S/p/b)
    Sign archive ? (Y/n)

Otherwise, it challenges the passphrase used to encrypt the file:

    gpg: AES256 encrypted data
    Enter passphrase:
    gpg: encrypted with 1 passphrase
    gpg: Signature made Tue Nov 22 13:15:03 2016 CET using RSA key ID XXXXXXXX
    gpg: Good signature from "test <test@example.com>"

SCARED! then opens a SHELL in the newly created directory:

    Editing file 'file'. Exit shell when done !
    test@test:path/to/your/file[SCARED!]$

When done with editing, exit:

    test@test:path/to/your/file[SCARED!]$ exit

SCARED! then requests for a commit confirmation and GPG parameters confirmation:

    Edit done. Commit, abort, edit ? (C/a/e)
    Gpg parameters :
      --s2k-cipher-algo AES256
      --s2k-digest-algo SHA512
      --s2k-count 65000000
      --symmetric --force-mdc
      --sign
    Commit, abort, edit ? (C/a/e)

Finally, it asks for passphrases for signature and encryption:

    You need a passphrase to unlock the secret key for
    user: "test <test@example.com>"
    4096-bit RSA key, ID XXXXXXXX, created 2016-11-08

    Enter passphrase: 
    gpg: AES256 encryption will be used
    Enter passphrase:
    Repeat passphrase:

### Security ###

SCARED! uses GnuPG to provide symmetric or public-key base encryption and signature of data.
It defaults to AES256 encryption with symmetric key (derived from a passphrase and a random salt).

It uses unshare to create a private, in memory (tmpfs), mounted directory.
